public class Factorial {

  public int factorial(int n) {
	  
    if(n < 0 || n > 12) {
    	  new IllegalArgumentException("value not in range");
    	 
    } else if(n == 0) {
    	
    	return 1;
    	
    } else {
    	
    	 return n * factorial(n-1);
    }
    return n;
  }
}
